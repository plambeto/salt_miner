<?php
// salt miner, version 1.3.3.7

if(isset($_GET['url'])) {
  $url = $_GET['url'];
} else {
  $url = "https://www.uniprot.org/uniprot/Q9W3R9.fasta";
}

$fasta_file = file_get_contents($url);
$fasta_file = substr( $fasta_file, strpos($fasta_file, "\n")+1 );
$fasta_file = trim(preg_replace('/\s*/', '', $fasta_file));

$fasta_array = str_split($fasta_file);
$amino_acids = str_split("MLIVFYWAGCSTPHNQKRED");

//var_dump($fasta_array);

function create_svg($array, $acids) {
  $width = sizeof($array) + 20;
  $height = sizeof($acids) * 10;
  header('Content-Type: image/svg+xml');
  print '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  print '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' . "\n";
  print '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="100%" height="100%" viewBox="0 0 ' . $width . ' ' . $height . '"' . ">\n";
?>

<style>
/* <![CDATA[ */
line {
  stroke: black;
  stroke-width: 1px;
}

text {
  font-size: 12px;
  font-family: "Lucida Console", Monaco, monospace;
}

/* ]]> */
</style>

<?php
  
  for ($i = 0; $i < sizeof($array); $i++) {
  //  print "<line x1='" . $i . "' x2='" . $i . "' y1='" . ($offset * 10) . "' y2='" . ($offset * 10 + 10) . "' class='line' />\n";
    print "<line x1='" . ($i + 0.5) . "' x2='" . ($i + 0.5) . "' y1='" . (array_search($array[$i], $acids) * 10) . "' y2='" . (array_search($array[$i], $acids) * 10 + 10) . "' />\n";
  }
  
  for ($j = 0; $j < sizeof($acids); $j++) {
    print "<text x='" . ($i + 10) . "' y='" . (($j + 1) * 10 - 1) . "'>" . $acids[$j] . "</text>\n";
  }
  
  print "</svg>\n";
  //print $i;
}

create_svg($fasta_array, $amino_acids);

//$fasta_array = array_reverse($fasta_array);

//create_svg($fasta_array, $amino_acids);

?>
